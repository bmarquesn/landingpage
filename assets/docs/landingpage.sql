-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.24 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para landingpage
CREATE DATABASE IF NOT EXISTS `landingpage` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `landingpage`;


-- Copiando estrutura para tabela landingpage.calculo_idade
CREATE TABLE IF NOT EXISTS `calculo_idade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sinal_de` varchar(2) NOT NULL,
  `idade_de` int(3) NOT NULL,
  `sinal_ate` varchar(2) NOT NULL,
  `idade_ate` varchar(3) NOT NULL,
  `pontos_negativos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela landingpage.calculo_idade: ~3 rows (aproximadamente)
DELETE FROM `calculo_idade`;
/*!40000 ALTER TABLE `calculo_idade` DISABLE KEYS */;
INSERT INTO `calculo_idade` (`id`, `sinal_de`, `idade_de`, `sinal_ate`, `idade_ate`, `pontos_negativos`) VALUES
	(1, '<', 17, '>', '100', 5),
	(2, '>=', 40, '<=', '99', 3),
	(3, '>=', 18, '<=', '39', 0);
/*!40000 ALTER TABLE `calculo_idade` ENABLE KEYS */;


-- Copiando estrutura para tabela landingpage.contato
CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidade` int(11) NOT NULL,
  `nome_completo` text NOT NULL,
  `data_nascimento` datetime NOT NULL,
  `email` text NOT NULL,
  `telefone` int(11) NOT NULL,
  `pontuacao` int(2) NOT NULL DEFAULT '10',
  `token` text,
  PRIMARY KEY (`id`),
  KEY `id_unidade` (`id_unidade`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela landingpage.contato: ~6 rows (aproximadamente)
DELETE FROM `contato`;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` (`id`, `id_unidade`, `nome_completo`, `data_nascimento`, `email`, `telefone`, `pontuacao`, `token`) VALUES
	(1, 0, 'Bruno ddd', '2016-07-14 00:00:00', 'bmarquesn@gmail.com', 2147483647, 10, NULL),
	(2, 0, 'Bruno Marques Nogueira', '1982-10-04 00:00:00', 'bmarquesn@gmail.com', 2147483647, 10, NULL),
	(3, 0, 'Novo Contato', '2000-07-01 00:00:00', 'aaa@aaa.com.br', 2147483647, 5, NULL),
	(4, 6, 'Bruno Marques Nogueira Atualizado', '1982-10-04 00:00:00', 'bmarquesn@gmail.com', 2147483647, 10, NULL),
	(5, 7, 'Bruno Atualizado aaa', '2000-07-01 00:00:00', 'bmarquesn@gmail.com', 2147483647, 1, 'a0657dc81606663a11f9ff27313b69a4'),
	(6, 3, 'Bruno Marques Nogueira', '1982-10-04 00:00:00', 'bruno.nomar@hotmail.com', 2147483647, 10, '69c7163e78f5e05eb4d06131fa886144'),
	(7, 4, 'Bruno Marques Nogueira Somente com Unidade', '1982-10-04 00:00:00', 'bmarquesn@gmail.com', 2147483647, 10, 'a0657dc81606663a11f9ff27313b69a4');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;


-- Copiando estrutura para tabela landingpage.regiao
CREATE TABLE IF NOT EXISTS `regiao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(12) NOT NULL,
  `pontos_negativos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela landingpage.regiao: ~5 rows (aproximadamente)
DELETE FROM `regiao`;
/*!40000 ALTER TABLE `regiao` DISABLE KEYS */;
INSERT INTO `regiao` (`id`, `nome`, `pontos_negativos`) VALUES
	(1, 'Sul', 2),
	(2, 'Sudeste', 1),
	(3, 'Centro-Oeste', 3),
	(4, 'Nordeste', 4),
	(5, 'Norte', 5);
/*!40000 ALTER TABLE `regiao` ENABLE KEYS */;


-- Copiando estrutura para tabela landingpage.unidade
CREATE TABLE IF NOT EXISTS `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_regiao` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_regiao` (`id_regiao`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela landingpage.unidade: ~8 rows (aproximadamente)
DELETE FROM `unidade`;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` (`id`, `id_regiao`, `nome`) VALUES
	(1, 1, 'Porto Alegre'),
	(2, 1, 'Curitiba'),
	(3, 2, 'São Paulo'),
	(4, 2, 'Rio de Janeiro'),
	(5, 2, 'Belo Horizonte'),
	(6, 3, 'Brasília'),
	(7, 4, 'Salvador'),
	(8, 4, 'Recife');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
