<?php echo doctype('html5'); ?>
<html lang="pt-br">
	<head>
		<?php require_once('estrutura/head.php'); ?>
	</head>
	<body>
		<?php require_once('estrutura/menu_topo.php'); ?>
		<div class="container">
			<div class="jumbotron">
				<br />
				<h1>Bruno Marques Nogueira</h1>
				<p>Landing Page, Gerente de Projetos, Desenvolvedor WEB</p>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-2">
					<h2>Contatos</h2>
					<p>Contatos cadastrados no Sistema</p>
					<p><a class="btn btn-success" href="<?php echo base_url().'admin/contato'; ?>">Contatos</a></p>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-2">
					<h2>Regiões / Pontuações</h2>
					<p>Regiões para os Estados Brasileitos</p>
					<p><a class="btn btn-success" href="<?php echo base_url().'admin/regiao/listar'; ?>">Regiões Brasileiras</a></p>
				</div>
			</div>
			<div class="clear"></div>
			<?php require_once('estrutura/assinatura_site.php'); ?>
		</div>
		<?php require_once('estrutura/footer.php'); ?>
	</body>
</html>
