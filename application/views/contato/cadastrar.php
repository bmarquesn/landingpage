<?php echo doctype('html5'); ?>
<html lang="pt-br">
	<head>
		<?php require_once(APPPATH.'views/estrutura/head.php'); ?>
		<style type="text/css">
		.ui-datepicker-title .ui-datepicker-year, .ui-datepicker-title .ui-datepicker-month {color:#000};
		</style>
	</head>
	<body>
		<?php require_once(APPPATH.'views/estrutura/menu_topo.php'); ?>
		<div class="container">
			<h1><?php echo $titulo_pagina; ?></h1>
			<p>Cadastrar Contato no sistema</p>
			<p class="alert bg-danger" style="display:none;"><strong>Mensagem!</strong> <span></span></p>
			<form action="" method="post" onsubmit="return validar_insercao_contato()">
				<input type="hidden" name="id" value="<?php echo isset($contato)&&!empty($contato)?$contato[0]->id:''; ?>" />
				<p><label for="nome">Nome</label><input type="text" id="nome" name="nome" value="<?php echo isset($contato)&&!empty($contato)?$contato[0]->nome_completo:''; ?>" class="form-control" /></p></label>
				<p><label for="dataNascimento">Data de Nascimento</label><input type="text" name="data_nascimento" value="<?php echo isset($contato)?date('d/m/Y H:i', strtotime($contato[0]->data_nascimento)):''; ?>" class="datepicker form-control" id="dataNascimento" /></p>
				<p><label for="email">E-mail</label><input type="text" id="email" name="email" value="<?php echo isset($contato)&&!empty($contato)?$contato[0]->email:''; ?>" class="form-control" /></p></label>
				<p><label for="telefone">Telefone</label><input type="text" id="telefone" name="telefone" value="<?php echo isset($contato)&&!empty($contato)?$contato[0]->telefone:''; ?>" class="form-control" /></p></label>
				<p class="regiao"><img src="<?php echo base_url().'assets/img/carregando.gif'; ?>" alt="Carregando" title="Carregando" style="display:none;" /><label for="regiao">Região</label>
					<select name="regiao" id="regiao" class="form-control">
						<option value="">-- Selecione --</option>
						<?php
						if(!empty($regiao)) {
							foreach($regiao as $key => $value) {
								echo '<option value="'.$value->id.'">'.$value->nome.'</option>';
							}
						}
						?>
					</select>
				</p>
				<p class="unidade">
					<label for="unidade">Unidade</label>
					<select name="unidade" id="unidade" class="form-control">
						<option value="">-- Selecione uma regiao --</option>
					</select>
				</p>
				<p><label for="token">Token</label><input type="text" id="token" name="token" value="<?php echo isset($contato)&&!empty($contato)?$contato[0]->token:''; ?>" class="form-control" /></p></label>
				<p><em><a href="" title="Solicitar Token" id="solicitar_token">Solicitar Token</a></em></p>
				<br />
				<p><input type="submit" value="Salvar" class="btn btn-default" /> <a href="<?php echo base_url().'admin/contato'; ?>" class="btn btn-default">Voltar</a></label>
			</form>
			<?php require_once(APPPATH.'views/estrutura/assinatura_site.php'); ?>
		</div>
		<div class="form_solicitar_token" style="display:none;"><form action="http://api.actualsales.com.br/join-asbr/ti/token" target="_blank" method="get"><input type="hidden" name="email" value="" /></form></div>
		<?php require_once(APPPATH.'views/estrutura/footer.php'); ?>
		<script type="text/javascript">
		$(function(){
			$("input#telefone").mask("(99) 9999-9999?9").focusout(function(event){
				var target,phone,element;
				target=(event.currentTarget)?event.currentTarget:event.srcElement;
				phone=target.value.replace(/\D/g,'');
				element=$(target);
				element.unmask();
				if(phone.length>10){
					element.mask("(99) 99999-999?9");
				}else{
					element.mask("(99) 9999-9999?9");
				}
			});
			$('input#dataNascimento').mask("99/99/9999");
			$('#nome').keyup(function () { 
				this.value = this.value.replace(/[^A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ .]/g,'');
			});
			$('#solicitar_token').click(function(){
				var email_preenchido=$('#email');
				if(email_preenchido.val()!=''&&validEmail(email_preenchido, 'E-MAIL')){
					$('.form_solicitar_token').find('form').find('input').val(email_preenchido.val());
					$('.form_solicitar_token').find('form').submit();
				}
			});
			$('#regiao').change(function(){
				var regiao_selecionada = $(this).val();
				if(regiao_selecionada!=''){
					$.ajax({
						type:"POST",
						data:{'regiao_selecionada':regiao_selecionada},
						url:base_url+'admin/regiao/',
						cache:'false',
						dataType: 'json',
						beforeSend: function(){
							$('p.regiao').find('img').fadeIn('fast');
						},
						complete: function(msg){
							$('p.regiao').find('img').fadeOut('fast');
						},
						success:function(data){
							var opcao='';
							if(data.length>0){
								opcao+='<option value="">-- Selecione uma unidade --</option>';
								for(i=0; i<data.length; i++){
									opcao+='<option value="'+data[i].id+'">'+data[i].nome+'</option>';
								}
							}else{
								opcao+='<option value="">-- Selecione uma regiao --</option>';
							}
							$('select#unidade').html(opcao);
						}
					});
				}
			});
		});
		function validar_insercao_contato(){
			var valido=false;
			var id = $('#id');
			var nome = $('#nome');
			var dataNascimento = $('#dataNascimento');
			var email = $('#email');
			var telefone = $('#telefone');
			var dataProximaAcao = $('#dataProximaAcao');
			var regiao = $('#regiao');
			var unidade = $('#unidade');
			if(nome.val()==''){
				completed(nome, 'NOME');
			}else if(dataNascimento.val()==''){
				completed(dataNascimento, 'DATA DE NASCIMENTO');
			}else if(email.val()==''){
				completed(email, 'E-MAIL');
			}else if(!validEmail(email, 'E-MAIL')){
				valido=false;
			}else if(telefone.val()==''){
				completed(telefone, 'TELEFONE');
			}else if(regiao.val()==''){
				completed(regiao, 'REGIÃO');
			}else if(unidade.val()==''){
				completed(unidade, 'UNIDADE');
			}else{
				var palavras=nome.val().split(' ');
				var qtd_palavras=0;
				for(i=0;i<palavras.length;i++){
					qtd_palavras++;
				}
				if(qtd_palavras<2){
					$('.alert.bg-danger span').html('O campo NOME deve ter no mínimo 2 palavras');
					$('.alert.bg-danger').show('fast');
					nome.focus();
					valido = false;
				}else{
					$('p.alert').hide('fast');
					valido = true;
				}
			}
			return valido;
		}
		</script>
	</body>
</html>