<?php echo doctype('html5'); ?>
<html lang="pt-br">
	<head>
		<?php require_once(APPPATH.'views/estrutura/head.php'); ?>
	</head>
	<body>
		<?php require_once(APPPATH.'views/estrutura/menu_topo.php'); ?>
		<div class="container">
			<h1><?php echo $titulo_pagina; ?></h1>
			<p>Contatos cadastrados no sistema</p>
			<p class="alert bg-danger" style="display:none;"><strong>Mensagem!</strong> <span></span></p>
			<p><a href="<?php echo base_url().'admin/contato/cadastrar'; ?>" class="btn btn-primary">Cadastrar</a></p>
			<table class="table table-bordered table-hover table-condensed tablesorter" id="contato">
				<thead>
					<tr>
						<th class="header">ID</th>
						<th class="header">Nome</th>
						<th class="header">Data de Nascimento</th>
						<th class="header">Email</th>
						<th class="header">Telefone</th>
						<th class="header">Pontuação</th>
						<th colspan="3" class="txtCenter">Ações</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if(!empty($contatos)) {
					foreach($contatos as $key => $value) {
						echo '
						<tr>
							<td>'.$value->id.'</td>
							<td>'.$value->nome_completo.'</td>
							<td>'.date('d/m/Y', strtotime($value->data_nascimento)).'</td>
							<td>'.$value->email.'</td>
							<td>'.$value->telefone.'</td>
							<td>'.$value->pontuacao.'</td>
							<td class="col-md-1"><a href="'.base_url().'admin/contato/cadastrar/'.$value->id.'" class="btn btn-default" title="Editar"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
							<td class="col-md-1"><a href="'.base_url().'admin/contato/excluir/'.$value->id.'" class="btn btn-danger" title="Excluir" onclick="return confirmar_exclusao(\'Contato\')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
							<td class="col-md-1"><img src="'.base_url().'assets/img/carregando.gif" alt="Carregando" style="display:none;" class="load_eads" /><a href="" class="btn btn-cauntion" title="Enviar Leads"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></a></td>
						</tr>
						';
					}
				} else {
					echo '<tr><td colspan="7"><em>Não há Contatos Cadastrados</em></td></tr>';
				} ?>
				</tbody>
				<?php if($total_registros > $itens_por_pagina) { ?>
				<tfoot>
					<tr>
						<td colspan="7"><?php echo $paginador; ?></td>
					</tr>
				</tfoot>
				<?php } ?>
			</table>
			<?php require_once(APPPATH.'views/estrutura/assinatura_site.php'); ?>
		</div>
		<?php require_once(APPPATH.'views/estrutura/footer.php'); ?>
		<div id="enviar_leads" style="display:none;">
			<form action="http://api.actualsales.com.br/join-asbr/ti/lead" method="post" target="_blank">
				<input type="hidden" name="nome" value="" />
				<input type="hidden" name="email" value="" />
				<input type="hidden" name="telefone" value="" />
				<input type="hidden" name="regiao" value="" />
				<input type="hidden" name="unidade" value="" />
				<input type="hidden" name="data_nascimento" value="" />
				<input type="hidden" name="score" value="" />
				<input type="hidden" name="token" value="" />
			</form>
		</div>
		<script type="text/javascript">
		$(function(){
			$('.glyphicon-send').click(function(){
				var botao_envio=$(this);
				var id_contato=$(this).parents('td').parent('tr').children('td:first').text();
				if(id_contato!=''){
					var confirmar=confirm('Deseja mesmo enviar LEADS deste contato?');
					if(confirmar){
						$.ajax({
							type:"POST",
							data:{'id_contato':id_contato},
							url:base_url+'admin/contato/get_dados_leads',
							cache:'false',
							dataType: 'json',
							beforeSend: function(){
								botao_envio.parents('td').find('img.load_eads').fadeIn('fast');
							},
							complete: function(msg){
								botao_envio.parents('td').find('img.load_eads').fadeOut('fast');
							},
							success:function(data){
								if(data!=''){
									$('div#enviar_leads').find('form').find('input[name="nome"]').val(data[0].nomeContato);
									$('div#enviar_leads').find('form').find('input[name="email"]').val(data[0].emailContato);
									$('div#enviar_leads').find('form').find('input[name="telefone"]').val(data[0].telefoneContato);
									$('div#enviar_leads').find('form').find('input[name="regiao"]').val(data[0].nomeRegiao);
									$('div#enviar_leads').find('form').find('input[name="unidade"]').val(data[0].nomeUnidade);
									$('div#enviar_leads').find('form').find('input[name="data_nascimento"]').val(data[0].dataNascimentoContato);
									$('div#enviar_leads').find('form').find('input[name="score"]').val(data[0].score);
									$('div#enviar_leads').find('form').find('input[name="token"]').val(data[0].token);
									if($('div#enviar_leads').find('form').find('input[name="token"]').val()!=''){
										$('div#enviar_leads').find('form').submit();
										botao_envio.parents('td').find('img.load_eads').fadeOut('fast');
										$('.alert.bg-danger span').html('LEADS enviados com sucesso');
										$('.alert.bg-danger').show('fast');
									}else{
										$('.alert.bg-danger span').html('Não há TOKEN para este cadastro');
										$('.alert.bg-danger').show('fast');
									}
								}
							}
						});
					}
				}
			});
		});
		</script>
	</body>
</html>