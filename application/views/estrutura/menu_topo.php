<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="container">
		<div class="navbar-header">
			<?php if(isset($_SESSION['admin']) && !empty($_SESSION['admin'])) { ?>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php } ?>
			<a class="navbar-brand" href="<?php echo base_url().'sistema'; ?>" title="Bruno Marques Nogueira - Gerente - Desenvolvedor - Projetos - Web">LEADS</a>
		</div>
		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="nav navbar-nav">
				<li<?php if(isset($pagina) && $pagina === 'contatos'){echo ' class="active"';}?>><a href="<?php echo base_url().'admin/contato'; ?>">Contatos</a></li>
				<li<?php if(isset($pagina) && $pagina === 'regioes_pontuacoes'){echo ' class="active"';}?>><a href="<?php echo base_url().'admin/regiao/listar'; ?>">Regiões Brasileiras</a></li>
			</ul>
		</div>
	</div>
</nav>