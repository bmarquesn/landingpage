<?php echo doctype('html5'); ?>
<html lang="pt-br">
	<head>
		<?php require_once(APPPATH.'views/estrutura/head.php'); ?>
		<style type="text/css">
		.ui-datepicker-title .ui-datepicker-year, .ui-datepicker-title .ui-datepicker-month {color:#000};
		</style>
	</head>
	<body>
		<?php require_once(APPPATH.'views/estrutura/menu_topo.php'); ?>
		<div class="container">
			<h1><?php echo $titulo_pagina; ?></h1>
			<p class="alert bg-danger" style="display:none;"><strong>Mensagem!</strong> <span></span></p>
			<form action="" method="post">
				<p>
					<label for="contatos">1º Selecione um contato:</label>
					<select name="contatos" id="contatos" class="form-control">
						<option value="">-- Selecione -- </option>
						<?php
						if(!empty($contatos)) {
							foreach($contatos as $key => $value) {
								echo '<option value="'.$value->id.'">'.$value->nome_completo.'</option>';
							}
						}
						?>
					</select>
				</p>
				
				<br />
				<p><input type="submit" value="Salvar" class="btn btn-default" /> <a href="<?php echo base_url().'admin/contato'; ?>" class="btn btn-default">Voltar</a></label>
			</form>
			<?php require_once(APPPATH.'views/estrutura/assinatura_site.php'); ?>
		</div>
		<?php require_once(APPPATH.'views/estrutura/footer.php'); ?>
		<script type="text/javascript">
		$(function(){
			
		});
		</script>
	</body>
</html>