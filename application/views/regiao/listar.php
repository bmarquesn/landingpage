<?php echo doctype('html5'); ?>
<html lang="pt-br">
	<head>
		<?php require_once(APPPATH.'views/estrutura/head.php'); ?>
	</head>
	<body>
		<?php require_once(APPPATH.'views/estrutura/menu_topo.php'); ?>
		<div class="container">
			<h1><?php echo $titulo_pagina; ?></h1>
			<p>Regiões e suas Unidades cadastrados no sistema</p>
			<table class="table table-bordered table-hover table-condensed tablesorter" id="contato">
				<thead>
					<tr>
						<th class="header">Nome Região</th>
						<th class="header">Pontos Negativos</th>
						<th class="header">Unidades</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if(!empty($regiao)) {
					foreach($regiao as $key => $value) {
						if(empty(trim($value->nomesUnidades))) {
							$value->nomesUnidades = '<em>Não há</em>';
						}
						
						echo '
						<tr>
							<td>'.$value->nomeRegiao.'</td>
							<td>'.$value->pontosNegativos.'</td>
							<td>'.$value->nomesUnidades.'</td>
						</tr>
						';
					}
				} else {
					echo '<tr><td colspan="7"><em>Não há Regioes Cadastradas</em></td></tr>';
				} ?>
				</tbody>
			</table>
			<?php require_once(APPPATH.'views/estrutura/assinatura_site.php'); ?>
		</div>
		<?php require_once(APPPATH.'views/estrutura/footer.php'); ?>
	</body>
</html>