<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** biblioteca para ser usada pelo sistema administrativo */
require_once(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'Admin.php');

class Contato extends Admin {
	public function __construct(){
		parent::__construct();
		$this->checarSessao = 0;
		$this->checarSessao();
		$this->load->model('Contato_model');
	}

	public function index($pagina = null, $msg = null) {
		$data['titulo_pagina'] = 'Contatos';
		$data['pagina'] = 'contato';
		$filtros = array();
		/** para paginacao estas variaveis são necessárias */
		$this->load->library('paginator');
		$config['page_query_string'] = TRUE;
		/** pega o numero das paginas */
		$pagina = $this->uri->segment_array();
		if(!empty($msg)) {
			/** retiro o elemento msg */
			$msg = array_pop($pagina);
			if(is_numeric(end($pagina))) {
				$pagina_sql = (int)end($pagina);
			}
		} else {
			if(is_numeric(end($pagina))) {
				$pagina_sql = (int)end($pagina);
			}
		}
		$pagina = !isset($pagina_sql)?0:$pagina_sql;
		$total_registros = count($this->Contato_model->get_all($this->Contato_model->tabela(), 0));
		$itens_por_pagina = $this->itensPorPagina;
		$paginador = $this->paginator->createPaginate('admin/contato/index/', $pagina, $total_registros, $itens_por_pagina, $config);
		$contatos = $this->Contato_model->listar($filtros, $pagina, $itens_por_pagina);
		
		if(!empty($filtros) && !empty($paginador)) {
			$paginador = $this->retorna_paginacao($filtros, $paginador);
		}
		
		$data['total_registros'] = $total_registros;
		$data['itens_por_pagina'] = $itens_por_pagina;
		$data['paginador'] = $paginador;
		$data['link_css'] = array('assets/css/tablesorter/style.css');
		$data['scripts_js'] = array('assets/js/tablesorter/jquery.tablesorter.js');
		$data['temTableSorter'] = 1;
		
		if(!empty($contatos)) {
			foreach($contatos as $key => $value) {
				$contatos[$key]->telefone = $this->mascara_string((string)$value->telefone, "(##) ####-####");
			}
		}
		
		$data['contatos'] = $contatos;
		
		if(!empty($msg)) {
			$data['msg'] = ucfirst(str_replace('_', ' ', $msg));
		} elseif(isset($_GET['msg']) && !empty(isset($_GET['msg']))) {
			$data['msg'] = ucfirst(str_replace('_', ' ', $_GET['msg']));
		}
		
		$this->load->view('contato/listar', $data);
	}

	/** Salva o Contato no BD */
	public function cadastrar($id = null) {
		/** insercao/atualizacao */
		if(isset($_POST['nome']) && !empty($_POST['nome']) && isset($_POST['email']) && !empty($_POST['email'])) {
			/** salvo os dados do Contato para pegar o ID */
			$data_nascimento = $_POST['data_nascimento'];
			$data['id'] = $this->anti_sql_injection($_POST['id']);
			$data['nome_completo'] = $this->anti_sql_injection($_POST['nome']);
			$data['data_nascimento'] = $this->anti_sql_injection($this->formatar_data_banco_dados($data_nascimento));
			$data['email'] = $this->anti_sql_injection($_POST['email']);
			$data['telefone'] = (int)$this->anti_sql_injection($this->somente_numeros($_POST['telefone']));
			$data['id_unidade'] = (int)$this->anti_sql_injection($_POST['unidade']);
			$data['token'] = $this->anti_sql_injection($_POST['token']);
			$data['pontuacao'] = 10;
			
			/** calcular a pontuacao pela idade */
			$idade_atual = $this->calcular_idade($data_nascimento);
			$this->load->model('Calculo_idade_model');
			
			$pontuacoes_idade = $this->Calculo_idade_model->get_all($this->Calculo_idade_model->tabela());
			
			if(!empty($pontuacoes_idade)) {
				foreach($pontuacoes_idade as $key => $value) {
					if((int)$value->idade_de < 18 || (int)$value->idade_de > 100) {
						if((int)$idade_atual < (int)$value->idade_de || (int)$idade_atual > (int)$value->idade_ate) {
							$data['pontuacao'] -= (int)$value->pontos_negativos;
						}
					} elseif((int)$idade_atual >= $value->idade_de && (int)$idade_atual <= $value->idade_ate) {
						$data['pontuacao'] -= (int)$value->pontos_negativos;
					}
				}
			}
			
			/** calcular a pontuacao pela regiao */
			$this->load->model('Regiao_model');
			$regioes = $this->Regiao_model->get_all($this->Regiao_model->tabela());
			
			/** quando Sao Paulo nao tirar pontos */
			$this->load->model('Unidade_model');
			$unidades = $this->Unidade_model->get_all($this->Unidade_model->tabela());
			$selecionou_sao_paulo = false;
			
			foreach($unidades as $key => $value) {
				if($value->nome === 'São Paulo' && (int)$value->id === $data['id_unidade']) {
					$selecionou_sao_paulo = true;
				}
			}
			
			foreach($regioes as $key => $value) {
				if($data['id_regiao'] === (int)$value->id) {
					if(!$selecionou_sao_paulo) {
						$data['pontuacao'] -= (int)$value->pontos_negativos;
					}
				}
			}
			
			
			if(!empty($data['id'])) {
				$this->Contato_model->upd_record($this->Contato_model->tabela(), $data);
				$id_contato = $data['id'];
			} else {
				$id_contato = $this->Contato_model->add_record($this->Contato_model->tabela(), $data);
			}
			if(!empty($id_contato)) {
				unset($_POST);
				if(!empty($data['id'])) {
					redirect(base_url().'admin/contato/index/0/contato_atualizado_com_sucesso');
				} else {
					redirect(base_url().'admin/contato/index/0/contato_cadastrado_com_sucesso');
				}
			} else {
				unset($_POST);
				redirect(base_url().'admin/contato/index/0/erro_ao_cadastrar_contato');
			}
		} else {
			if(!empty($id)) {
				$data['titulo_pagina'] = 'Editar';
			} else {
				$data['titulo_pagina'] = 'Cadastrar';
			}
			
			$data['pagina'] = 'contato';
			
			$this->load->model('Regiao_model');
			$data['regiao'] = $this->Regiao_model->get_all($this->Regiao_model->tabela(), 'nome', null, 'ASC');
			
			$this->load->model('Unidade_model');
			$data['unidade'] = $this->Unidade_model->get_all($this->Unidade_model->tabela(), 'nome', null, 'ASC');
			
			if(!empty($id)) {
				/** dados edicao */
				$data['contato'] = $this->Contato_model->get_all_where($this->Contato_model->tabela(), 'id', $this->anti_sql_injection($id));
			}
			
			$data['scripts_js'] = array('assets/js/jquery/jquery.maskedinput.js');

			$this->load->view('contato/cadastrar', $data);
		}
	}
	
	public function excluir($id = null) {
		if($this->Contato_model->del_record($this->Contato_model->tabela(), $id)) {
			redirect(base_url().'admin/contato/index/0/contato_excluido_com_sucesso');
		} else {
			redirect(base_url().'admin/contato/index/0/erro_ao_excluir_contato');
		}
	}
	
	public function get_dados_leads() {
		$id_contato = isset($_POST['id_contato'])?(int)$this->anti_sql_injection($_POST['id_contato']):0;
		
		$dados_leads = $this->Contato_model->dados_leads($id_contato);
		
		echo json_encode($dados_leads);
		exit;
	}
}
