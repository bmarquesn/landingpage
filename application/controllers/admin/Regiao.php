<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** biblioteca para ser usada pelo sistema administrativo */
require_once(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'Admin.php');

class Regiao extends Admin {
	public function __construct(){
		parent::__construct();
		$this->checarSessao = 0;
		$this->checarSessao();
		$this->load->model('Regiao_model');
	}

	public function index() {
		$regiao_selecionada = isset($_POST['regiao_selecionada'])?(int)$this->anti_sql_injection($_POST['regiao_selecionada']):0;
		
		$this->load->model('Unidade_model');
		$unidades_regiao = $this->Unidade_model->get_all_where($this->Unidade_model->tabela(), 'id_regiao', $regiao_selecionada);
		
		echo json_encode($unidades_regiao);
	}
	
	public function listar(){
		$data['titulo_pagina'] = 'Regiões Brasileiras';
		$data['pagina'] = 'regiao';
		$data['regiao'] = $this->Regiao_model->listar();
		
		$this->load->view('regiao/listar', $data);
	}
}
