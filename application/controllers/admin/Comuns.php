<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

session_start();
date_default_timezone_set("America/Sao_Paulo");

class Comuns extends CI_Controller {
	/*funcoes, metodos comuns para o sistema*/
	protected $hash_senha = "_3ncr1pt*p455w0rd_";

	public function hash_senha() {
		return $this->hash_senha;
	}

	public function formatar_data_banco_dados($data, $hora = false) {
		if($hora) {
			$dataFormatada = substr($data, 6, 4)."-".substr($data, 3, 2)."-".substr($data, 0, 2)." ".substr($data, 11, 5);
		} else {
			$dataFormatada = substr($data, 6, 4)."-".substr($data, 3, 2)."-".substr($data, 0, 2);
		}
		return $dataFormatada;
	}
	
	public function anti_sql_injection($sql, $array=false) {
		if($array){
			foreach($sql as $k => $c) {
				$c = preg_replace("/(from|select|insert|delete|truncate|where|drop table|show tables|#|\*|--|\\\\)/","",$c);
				$c = trim($c);
				$c = strip_tags($c);
				$c = get_magic_quotes_gpc()==0?addslashes($c):$c;
				$sql[$k] = $c;
				return $sql;
			}
		}else{
			$sql = preg_replace("/(from|select|insert|delete|truncate|where|drop table|show tables|#|\*|--|\\\\)/","",$sql);
			$sql = trim($sql);
			$sql = strip_tags($sql);
			$sql = get_magic_quotes_gpc()==0?addslashes($sql):$sql;
			return $sql;
		}
	}
	
	public function remover_acentuacao($string) {
		$tr = strtr($string, array (
			'À'=>'A','Á'=>'A','Â'=>'A','Ã'=>'A','Ä'=>'A','Å'=>'A',
			'Æ'=>'A','Ç'=>'C','È'=>'E','É'=>'E','Ê'=>'E','Ë'=>'E',
			'Ì'=>'I','Í'=>'I','Î'=>'I','Ï'=>'I','Ð'=>'D','Ñ'=>'N',
			'Ò'=>'O','Ó'=>'O','Ô'=>'O','Õ'=>'O','Ö'=>'O','Ø'=>'O',
			'Ù'=>'U','Ú'=>'U','Û'=>'U','Ü'=>'U','Ý'=>'Y','Ŕ'=>'R',
			'Þ'=>'s','ß'=>'B','à'=>'a','á'=>'a','â'=>'a','ã'=>'a',
			'ä'=>'a','å'=>'a','æ'=>'a','ç'=>'c','è'=>'e','é'=>'e',
			'ê'=>'e','ë'=>'e','ì'=>'i','í'=>'i','î'=>'i','ï'=>'i',
			'ð'=>'o','ñ'=>'n','ò'=>'o','ó'=>'o','ô'=>'o','õ'=>'o',
			'ö'=>'o','ø'=>'o','ù'=>'u','ú'=>'u','û'=>'u','ý'=>'y',
			'þ'=>'b','ÿ'=>'y','ŕ'=>'r',' '=>'-','Ü'=>'U','ü'=>'u',
			'['=>'',']'=>'','['=>'','>'=>'','<'=>'','}'=>'','{'=>'',
			')'=>'','('=>'',':'=>'',';'=>'',','=>'','!'=>'','?'=>'',
			'*'=>'','%'=>'','~'=>'','^'=>'','`'=>'','&'=>'','#'=>'','@'=>'','_'=>'-',"'"=>''
		));
		
		return $tr;
	}
	
	function mascara_string($val, $mask) {
		$maskared = '';
		$k = 0;
		for ($i = 0; $i <= strlen($mask) - 1; $i++) {
			if ($mask[$i] == '#') {
				if (isset ($val[$k]))
					$maskared .= $val[$k++];
			} else {
				if (isset ($mask[$i]))
					$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}
	
	public function traz_estados_brasileiros() {
		$estadosBrasileiros = array(
			'AC' => 'Acre',
			'AL' => 'Alagoas',
			'AM' => 'Amazonas',
			'AP' => 'Amapá',
			'BA' => 'Bahia',
			'CE' => 'Ceará',
			'DF' => 'Distrito Federal',
			'ES' => 'Espírito Santo',
			'GO' => 'Goiás',
			'MA' => 'Maranhão',
			'MG' => 'Minas Gerais',
			'MS' => 'Mato Grosso do Sul',
			'MT' => 'Mato Grosso',
			'PA' => 'Pará',
			'PB' => 'Paraíba',
			'PE' => 'Pernambuco',
			'PI' => 'Pauí',
			'PR' => 'Paraná',
			'RJ' => 'Rio de Janeiro',
			'RN' => 'Rio Grande do Norte',
			'RO' => 'Rondônia',
			'RR' => 'Roraima',
			'RS' => 'Rio Grande do Sul',
			'SC' => 'Santa Catarina',
			'SE' => 'Sergipe',
			'SP' => 'São Paulo',
			'TO' => 'Tocantins'
		);
		
		return $estadosBrasileiros;
	}
	
	public function nome_meses($num_mes = null) {
		$meses = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Março',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		if(!empty($num_mes)) {
			return $meses[(int)$num_mes];
		} else {
			return $meses;
		}
	}
	
	public function diferenca_entre_datas($data1, $data2) {
		/** utiliza-se o formato Y-m-d H:i:s */
		$ano1 = date('Y', strtotime($data1));
		$mes1 = date('m', strtotime($data1));
		$dia1 = date('d', strtotime($data1));
		$hora1 = date('H', strtotime($data1));
		$minuto1 = date('i', strtotime($data1));
		$segundo1 = date('s', strtotime($data1));
		
		$ano2 = date('Y', strtotime($data2));
		$mes2 = date('m', strtotime($data2));
		$dia2 = date('d', strtotime($data2));
		$hora2 = date('H', strtotime($data2));
		$minuto2 = date('i', strtotime($data2));
		$segundo2 = date('s', strtotime($data2));
		
		$mktime1 = mktime($hora1, $minuto1, $segundo1, $mes1, $dia1, $ano1);
		$mktime2 = mktime($hora2, $minuto2, $segundo2, $mes2, $dia2, $ano2);
		
		$dias = floor(($mktime2-$mktime1)/86400);
		$meses = floor(($mktime2-$mktime1)/2592000);
		$anos = floor(($mktime2-$mktime1)/31536000);
		$horas = floor(($mktime2-$mktime1)/3600);
		$minutos = floor(($mktime2-$mktime1)/60);
		$segundos = floor(($mktime2-$mktime1)/1);
		
		$arrayDiferencaDatas = array('dias' => $dias, 'meses' => $meses, 'anos' => $anos, 'horas' => $horas, 'minutos' => $minutos, 'segundos' => $segundos);
		
		return $arrayDiferencaDatas;
	}
	
	public function download_arquivo($id_webinar, $nome_arquivo) {
		switch(strtolower(substr(strrchr(basename($nome_arquivo),"."),1))) {
			case "pdf": $tipo="application/pdf"; break;
			case "exe": $tipo="application/octet-stream"; break;
			case "zip": $tipo="application/zip"; break;
			case "doc": $tipo="application/msword"; break;
			case "docx": $tipo="application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
			case "xls": $tipo="application/vnd.ms-excel"; break;
			case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
			case "pptx": $tipo="application/vnd.openxmlformats-officedocument.presentationml.presentation"; break;
			case "gif": $tipo="image/gif"; break;
			case "png": $tipo="image/png"; break;
			case "jpg": $tipo="image/jpg"; break;
			case "mp3": $tipo="audio/mpeg"; break;
			case "php": // deixar vazio por seurança
			case "htm": // deixar vazio por seurança
			case "html": // deixar vazio por seurança
		}
		if(isset($tipo) && !empty($tipo)) {
			header("Content-Type: ".$tipo);
			header("Content-Length: ".filesize('assets/docs/ppt/'.$id_webinar.'/'.$nome_arquivo));
			header("Content-Disposition: attachment; filename=".basename('assets/docs/ppt/'.$id_webinar.'/'.$nome_arquivo));
			readfile('assets/docs/ppt/'.$id_webinar.'/'.$nome_arquivo);
		}
		exit;
	}

	public function detectar_tipo_dispositivo() {
		$agent = $_SERVER["HTTP_USER_AGENT"];
		if(preg_match("/iPhone/", $agent) || preg_match("/iPad/", $agent)) {
			$dispositivo = 'IPhone';
			//header("Location: http://".$url_stream.":1935/".$ponto_stream."/".$name_stream_ptbr."/playlist.m3u8");
		} elseif((preg_match("/Android/", $agent))) {
			$dispositivo = 'Android';
		} else {
			$dispositivo = 'PC';
		}
		
		return $dispositivo;
	}

	function geradorSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false) {
		$lmin = 'abcdefghijklmnopqrstuvwxyz';
		$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '1234567890';
		$simb = '!@#$%*-';
		
		$retorno = '';
		$caracteres = '';
		$caracteres .= $lmin;
		
		if($maiusculas) {
			$caracteres .= $lmai;
		}
		
		if($numeros) {
			$caracteres .= $num;
		}
		
		if($simbolos) {
			$caracteres .= $simb;
		}
		
		$len = strlen($caracteres);
		
		for ($n = 1; $n <= $tamanho; $n++) {
			$rand = mt_rand(1, $len);
			$retorno .= $caracteres[$rand-1];
		}
		
		return $retorno;
	}
	
	function valida_email($email) {
		$valido = false;
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$valido = true;
		} else {
			$valido = false;
		}
		
		return $valido;
	}
	
	public function retorna_paginacao($filtros, $paginador) {
		$paginador = str_replace('class="btn btn-info"', 'class="btn"', $paginador);
		$paginador = explode('<a href="', $paginador);
		foreach($paginador as $key => $value) {
			if($key > 0) {
				$links_paginador[$key] = explode('"><button class="btn', $value);
			}
		}
		if(isset($links_paginador) && !empty($links_paginador) && isset($filtros) && !empty($filtros)) {
			foreach($filtros as $key3 => $value3) {
				foreach($links_paginador as $key => $value) {
					foreach($value as $key2 => $value2) {
						if($key2 == 0) {
							$links_paginador[$key][$key2] = $value2.'&'.$key3.'='.$value3;
						}
					}
				}
			}
			foreach($links_paginador as $key => $value) {
				foreach($value as $key2 => $value2) {
					if($key2 == '1') {
						$links_paginador[$key][$key2] = explode('">', $value2);
					}
				}
			}
			foreach($links_paginador as $key => $value) {
				foreach($value as $key2 => $value2) {
					if($key2 == '0') {
						$links_paginador[$key][0] = '<a href="'.$value2.'"><button class="btn">';
					}
				}
			}
			foreach($links_paginador as $key => $value) {
				$links_paginador[$key][0] = $value[0].$value[1][1];
			}
			$string_paginador = '';
			foreach($paginador as $key => $value) {
				if($key > 0) {
					$string_paginador .= $links_paginador[$key][0];
				}
			}
			$string_paginador = $paginador[0].$string_paginador;
			return $string_paginador;
		}
	}
	
    public function somente_numeros($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }
	
	public function calcular_idade($idade_contato) {
		/** senha $idade_contato = m/d/Y */
		list($dia, $mes, $ano) = explode('/', $idade_contato);
	   
		//$hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
		$hoje = mktime(0, 0, 0, '06', '01', '2016');
		$nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
	   
		$idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
		
		return $idade;
	}
}