<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'Sistema.php');

class Leads extends Sistema {
	public function __construct(){
		parent::__construct();
		$this->checarSessao = 0;
		$this->checarSessao();
		$this->load->model('Contato_model');
	}
	
	public function index() {
		$data['titulo_pagina'] = 'Envio de Leads';
		$data['pagina'] = 'envio_leads';
		
		$data['contatos'] = $this->Contato_model->get_all($this->Contato_model->tabela(), 0);
		
		$this->load->view('envio_leads', $data);
	}
}
