<?php
require_once('Comuns_model.php');

class Contato_model extends Comuns_model {
	protected $tabela = 'contato';
	
	public function listar($filtros, $pagina = '', $limit = '') {
		$select = 'Contato.*';
		$this->db->select($select);
		
		if(!empty($filtros)) {
			if(isset($filtros['nome_completo'])) {
				$this->db->where('Contato.nome_completo LIKE(\'%%'.$filtros['nome_completo'].'%%\')');
			}
		}
		
		$this->db->group_by('Contato.id');
		$this->db->order_by('Contato.nome_completo ASC');
		
		if($pagina !== '' && $limit !== '') {
			$this->db->limit($limit, $pagina);
		}
		
		$data = $this->db->get($this->tabela.' AS Contato');
		return $data->result();
	}
	
	public function dados_leads($id_contato) {
		$this->db->select('
			Contato.nome_completo AS nomeContato
			,Contato.email AS emailContato
			,Contato.telefone AS telefoneContato
			,Regiao.nome AS nomeRegiao
			,Unidade.nome AS nomeUnidade
			,DATE_FORMAT(Contato.data_nascimento, \'%Y-%m-%d\') AS dataNascimentoContato
			,Contato.pontuacao AS score
			,Contato.token AS token
		');
		$this->db->join('unidade AS Unidade', 'Unidade.id = Contato.id_unidade', 'LEFT');
		$this->db->join('regiao AS Regiao', 'Regiao.id = Unidade.id_regiao', 'LEFT');
		$this->db->where('Contato.id', $id_contato);
		
		$data = $this->db->get($this->tabela.' AS Contato');
		
		return $data->result();
	}
}