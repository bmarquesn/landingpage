<?php
require_once('Comuns_model.php');

class Regiao_model extends Comuns_model {
	protected $tabela = 'regiao';
	
	public function listar() {
		$this->db->select('
			Regiao.id AS idRegiao
			,Regiao.nome AS nomeRegiao
			,Regiao.pontos_negativos AS pontosNegativos
			,GROUP_CONCAT(DISTINCT(Unidade.id) ORDER BY Unidade.nome) AS idsUnidades
			,GROUP_CONCAT(DISTINCT(Unidade.nome) ORDER BY Unidade.nome) AS nomesUnidades
		');
		$this->db->join('unidade AS Unidade', 'Unidade.id_regiao = Regiao.id', 'LEFT');
		$this->db->group_by('Regiao.id');
		$this->db->order_by('Regiao.nome');
		
		$data = $this->db->get($this->tabela.' AS Regiao');
		
		return $data->result();
	}
}